package com.app.interconnect.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.interconnect.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by hp on 28-02-2017.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.OrderListViewHolder> {

    List<HashMap<String, String>> orderListModels;

    private Context context;

    public RecyclerViewAdapter(Context context, List<HashMap<String, String>> orderListModels) {

        this.orderListModels = orderListModels;
        this.context = context;
    }


    @Override
    public OrderListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_layout, parent, false);

        return new OrderListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderListViewHolder holder, final int position) {
        String date=orderListModels.get(position).get("date");
        String msg = orderListModels.get(position).get("msg");
        String reply = orderListModels.get(position).get("replay");
        String name = orderListModels.get(position).get("name");
        holder.tv_name.setText(name);
        holder.tv_date.setText(date);
        holder.tv_msg.setText(msg);
        holder.tv_replay.setText(reply);
//        holder.tvTotalCBM.setText(orderListModels.get(position).getTotalOrderCBM());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }

        });


    }



    @Override
    public int getItemCount() {
        return orderListModels.size();
    }

    class OrderListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_name, tv_date, tv_msg, tv_replay;
        public OrderListViewHolder(View itemView) {
            super(itemView);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_date=(TextView)itemView.findViewById(R.id.tv_dateList);
            tv_msg=(TextView)itemView.findViewById(R.id.tv_message);
            tv_replay = (TextView)itemView.findViewById(R.id.tv_replay);
        }

        @Override
        public void onClick(View view) {

        }
    }
}