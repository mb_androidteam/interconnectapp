package com.app.interconnect.Adapters;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.interconnect.R;
import com.app.interconnect.Utils.UserPref;
import com.app.interconnect.Utils.VollyErrorCatcher;
import com.app.interconnect.app.AppController;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hp on 15-03-2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.OrderListViewHolder> {

    List<HashMap<String, String>> addressList;
    List<HashMap<String, String>> adrl = null;
    HashMap<String, String> adress;
    RecyclerView rv_list;
    private LinearLayoutManager layoutManager;
    RecyclerViewAdapter adapt;
    VollyErrorCatcher vollyErrorCatcher;
    UserPref up;
    Dialog dialog;
    ProgressDialog pd;

    private Context context;

    public NotificationAdapter(Context context, List<HashMap<String, String>> addressList) {

        this.addressList = addressList;
        this.context = context;
        vollyErrorCatcher = new VollyErrorCatcher();
        up = new UserPref(context);
        pd = new ProgressDialog(context);
        pd.setMessage("Loading...");
    }


    @Override
    public NotificationAdapter.OrderListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.notification_item, parent, false);

        return new NotificationAdapter.OrderListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.OrderListViewHolder holder, final int position) {
        final String id=addressList.get(position).get("msgid");
        final String date =addressList.get(position).get("date");
        holder.tv_description.setText(addressList.get(position).get("description"));
        holder.tv_date.setText(date);
        holder.tv_place.setText(addressList.get(position).get("place"));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                lisDetail(id,context);

            }

        });


    }



    @Override
    public int getItemCount() {
        return addressList.size();
    }

    class OrderListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_place, tv_date, tv_description, tv_replay;
        public OrderListViewHolder(View itemView) {
            super(itemView);
            tv_place = (TextView) itemView.findViewById(R.id.tv_place);
            tv_date=(TextView)itemView.findViewById(R.id.tv_date);
            tv_description=(TextView)itemView.findViewById(R.id.tv_description);
        }

        @Override
        public void onClick(View view) {


        }
    }
    public void lisDetail(String id, final Context contextT){
        pd.show();
        String URL_msgDetail = "http://54.202.4.169/api/v1/getmessage?appVersion=1.0&apiVersion=1.0&messageid=" + id;
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL_msgDetail, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    System.out.println("response" + response);
                    int resultcode = Integer.parseInt(response.getString("resultCode").toString());
                    addressList = new ArrayList<HashMap<String, String>>();
                    JSONArray searchArray = response.getJSONArray("messageResponse");
                    if (resultcode == 1) {
                        JSONObject msgDetails = response.getJSONObject("messageDetails");//3types of msg
                        Double lati = (Double) msgDetails.get("Latitude");
                        Double longi = (Double) msgDetails.get("Longitude");
                        String incomingPlace = (String) msgDetails.get("Place");
                        LatLng makLatLng = new LatLng(lati, longi);

                        final Dialog dialog = new Dialog(contextT);
                        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                        DisplayMetrics display = contextT.getResources().getDisplayMetrics();
                        int width = display.widthPixels;
                        int height = display.heightPixels;
                        dialog.setContentView(R.layout.message_list);
                        dialog.setCanceledOnTouchOutside(false);
                        TextView tv =(TextView)dialog.findViewById(R.id.tv_NoMsg);
                        ListView lv =(ListView) dialog.findViewById(R.id.rv_feed);

                        ListView rvagentdetailslist = (ListView) dialog.findViewById(R.id.rv_feed);


                        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
                        if(searchArray.length()>0){
                            tv.setVisibility(View.GONE);
                            lv.setVisibility(View.VISIBLE);
                        }
                        else {
                            tv.setVisibility(View.VISIBLE);
                            lv.setVisibility(View.GONE);
                        }

                        for (int i = 0; i < searchArray.length(); i++) {
                            JSONObject test = searchArray.getJSONObject(i);
                            adress = new HashMap<String, String>();
                            String name = test.getString("FirstName");
                            String date = test.getString("UpdatedOn");
                            String msg = test.getString("Response");
                            String reply = test.getString("ReplyMessage");
                            HashMap<String, String> hashMap = new HashMap<>();//create a hashmap to store the data in key value pair
                            hashMap.put("name", test.getString("FirstName"));
                            hashMap.put("date", test.getString("UpdatedOn"));
                            hashMap.put("response", test.getString("Response"));
                            hashMap.put("reply", test.getString("ReplyMessage"));
                            arrayList.add(hashMap);//add the hashmap into arrayList
                        }

                        String[] from = {"name", "date", "response", "reply"};//string array
                        int[] to = {R.id.tv_name, R.id.tv_dateList, R.id.tv_message, R.id.tv_replay};//int array of views id's
                        SimpleAdapter simpleAdapter = new SimpleAdapter(contextT, arrayList, R.layout.recycler_layout, from, to);//Create object and set the parameters for simpleAdapter
                        rvagentdetailslist.setAdapter(simpleAdapter);//sets the adapter for listView
                        dialog.getWindow().setLayout((4 * width) / 4, LinearLayout.LayoutParams.WRAP_CONTENT);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                        dialog.show();
                        pd.dismiss();

                    } else {
                        pd.dismiss();
                    }

                } catch (JSONException e) {
                    pd.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                int res = error.networkResponse.statusCode;
                pd.dismiss();
                if(res==401) {
//                    vollyErrorCatcher.StatusCodeChecker(res,sd);
                }
            }


        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                String token = up.GetICtoken().toString();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjReq, "sendMessage");

    }

}