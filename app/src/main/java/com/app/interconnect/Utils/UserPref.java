package com.app.interconnect.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by hp on 20-02-2017.
 */

public class UserPref {
    SharedPreferences sp;
    Context activity;

    public UserPref(Context context) {
        activity = context;
        sp = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void SaveFCMtoken(String id) {
        SharedPreferences.Editor token = sp.edit();
        token.putString("token", id);
        token.commit();
    }

    public String GetFCMtoken() {
        String token = sp.getString("token", "0");
        return token;
    }
    public void SaveUserName(String uname){
        SharedPreferences.Editor userName = sp.edit();
        userName.putString("uname", uname);
        userName.commit();
    }
    public String GetUserName() {
        String uname = sp.getString("uname", "0");
        return uname;
    }
    public void SaveICtoken(String id) {
        SharedPreferences.Editor token = sp.edit();
        token.putString("icToken", id);
        token.commit();
    }
    public String GetICtoken() {
        String token = sp.getString("icToken", "0");
        return token;
    }
    public void Clear(){
       SharedPreferences.Editor ed = sp.edit();
        ed.clear();
        ed.commit();
    }
}