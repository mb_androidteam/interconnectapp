package com.app.interconnect.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.interconnect.R;
import com.app.interconnect.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hp on 21-02-2017.
 */

public class ResponseDialog {
    String URL="http://54.202.4.169/api/v1/sendreply";
    VollyErrorCatcher vollyErrorCatcher;
    Activity act;
    String postiv="",negative="",Message="";
    Button btn_Positive, btn_negative;
    TextView tv_Description;
    EditText replyMessage;
    ProgressDialog pd;
    public void showDialog(final Activity activity, final int MsgTypeId, final  int msgId, String descrip) {
        act=activity;
        pd = new ProgressDialog(activity);
        pd.setMessage("Retriving..");
        vollyErrorCatcher= new VollyErrorCatcher();
        switch (MsgTypeId)
        {
            case 1:
                postiv="Yes";
                negative="No";
                Message="Am i safe here";
                break;
            case 2:
                postiv="Ok";
                negative="Not Ok";
                Message="Ok not-ok message";
                break;
            case 3:
                postiv="Safe";
                negative="Not Safe";
                Message="Safe not Safe message";
        }
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.custom_dialog_response);
        btn_negative = (Button)dialog.findViewById(R.id.btn_Negative);
        btn_Positive = (Button)dialog.findViewById(R.id.btn_Postive);
        tv_Description = (TextView)dialog.findViewById(R.id.Description);
        replyMessage = (EditText)dialog.findViewById(R.id.edtReplayMessage);
        tv_Description.setText(descrip);
        btn_negative.setText(negative);
        btn_Positive.setText(postiv);

        dialog.show();
        btn_negative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String reply = replyMessage.getText().toString();
                try{
                    UpdateDb(msgId,negative,reply);
                    dialog.dismiss();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        btn_Positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String reply = replyMessage.getText().toString();
                try{
                    UpdateDb(msgId,postiv,reply);
                    dialog.dismiss();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });


    }
    void UpdateDb(int msgId,String Response, String message)
    {
        pd.show();
        final JSONObject params = new JSONObject();
        try {
            params.put("appVersion", "1.0");
            params.put("apiVersion", "1.0");
            params.put("messageid", msgId);
            params.put("messageresponse", Response);
            params.put("messagereply",message);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URL, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    System.out.println("response" + response);
                    int resultcode = Integer.parseInt(response.getString("resultCode").toString());
                    if(resultcode==1)
                    {
                        System.out.println("Sent Sucessfully");
                        pd.dismiss();
                    }
                    else {
                        pd.dismiss();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                if(error.networkResponse==null)
                {
                    Toast.makeText(act, "No internet Connection", Toast.LENGTH_SHORT).show();
                }
                else {
                    int res = error.networkResponse.statusCode;
                    if (res == 401) {
                        vollyErrorCatcher.StatusCodeChecker(res, act);
                    }
                }
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                UserPref up = new UserPref(act);
                String token = up.GetICtoken().toString();
                headers.put("Authorization", "Bearer " + token);
                System.out.println("Token:"+token);
                return headers ;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjReq, "sendMessageReplay");

    }
}
