package com.app.interconnect.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.menu.ExpandedMenuView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.interconnect.R;
import com.app.interconnect.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hp on 20-02-2017.
 */

public class SendConnectDialog {
    int messageType = 0;
    VollyErrorCatcher vollyErrorCatcher;
    String URL = "http://54.202.4.169/api/v1/sendmessage";
    Spinner spinner;
    EditText edtMessage;
    Button btnConnect;
    ProgressDialog pd;

    public void showDialog(final Activity activity, final String locationName, final double latitude, final double longitude, final String location, final String place) {

        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.custom_dialog);
        edtMessage = (EditText) dialog.findViewById(R.id.edtMessage);
        btnConnect = (Button) dialog.findViewById(R.id.btn_Connect);
        pd= new ProgressDialog(activity);
        pd.setMessage("Sending...");

        spinner = (Spinner) dialog.findViewById(R.id.Spinner_MsgType);

        dialog.show();
        btnConnect.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View view) {
                                              pd.show();
                                              String strName = spinner.getSelectedItem().toString();
                                              String msg = edtMessage.getText().toString();
                                              if (msg.equals("")) {
                                                  msg = "No message";
                                              }
                                              switch (strName) {
                                                  case "OK/NotOK":
                                                      messageType = 1;
                                                      break;
                                                  case "Yes/No":
                                                      messageType = 2;
                                                      break;
                                                  case "Safe/Not Safe":
                                                      messageType = 3;
                                                      break;
                                                  default:
                                                      messageType = 1;
                                                      break;
                                              }
                                              Toast.makeText(activity, "Message Send", Toast.LENGTH_SHORT).show();
                                              final JSONObject params = new JSONObject();
                                              try {
                                                  params.put("appVersion", "1.0");
                                                  params.put("apiVersion", "1.0");
                                                  params.put("longitude", longitude);
                                                  params.put("latitude", latitude);
                                                  params.put("place", place);
                                                  params.put("messagetype", messageType);
                                                  params.put("location", location);
                                                  params.put("description", msg);
                                                  params.put("subject", "Subject will be added soon");

                                              } catch (JSONException e) {
                                                  e.printStackTrace();
                                              }
                                              JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URL, params, new Response.Listener<JSONObject>() {

                                                  @Override
                                                  public void onResponse(JSONObject response) {
                                                      try {
                                                          System.out.println("response" + response);
                                                          int resultcode = Integer.parseInt(response.getString("resultCode").toString());
                                                          if (resultcode == 1) {
                                                              Toast.makeText(activity, "Connected..Stay Safe", Toast.LENGTH_SHORT).show();
                                                              pd.dismiss();
                                                          } else {
                                                              pd.dismiss();
                                                          }
                                                          dialog.dismiss();

                                                      } catch (JSONException e) {
                                                          e.printStackTrace();
                                                          pd.dismiss();
                                                      }
                                                  }
                                              }, new Response.ErrorListener() {

                                                  @Override
                                                  public void onErrorResponse(VolleyError error) {
                                                      error.printStackTrace();
                                                      if(error.networkResponse==null)
                                                      {
                                                          Toast.makeText(activity, "No internet Connection", Toast.LENGTH_SHORT).show();
                                                      }
                                                      else {
                                                          int res = error.networkResponse.statusCode;
                                                          if (res == 401) {
                                                              vollyErrorCatcher.StatusCodeChecker(res, activity);
                                                          }
                                                      }
                                                  }

                                              }) {
                                                  @Override
                                                  public Map<String, String> getHeaders() throws AuthFailureError {
                                                      HashMap<String, String> headers = new HashMap<String, String>();
                                                      UserPref up = new UserPref(activity);
                                                      String token = up.GetICtoken().toString();
                                                      headers.put("Authorization", "Bearer " + token);
                                                      return headers;
                                                  }
                                              };

                                              AppController.getInstance().addToRequestQueue(jsonObjReq, "sendMessage");
                                          }

                                      }
        );


    }

}



