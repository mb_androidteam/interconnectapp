package com.app.interconnect.Utils;

import android.app.ProgressDialog;
import android.content.Intent;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.interconnect.Activity.GoogleSignIn;
import com.app.interconnect.Activity.MainActivity;
import com.app.interconnect.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by hp on 22-02-2017.
 */

public class ApiCommonClass {
    UserPref up;
    ProgressDialog pDialog;
    int UpdateLocationcode;
    public int UpdateUserlocation(double latitude, double longitude, String place, String location) {

        String URL = "http://54.202.4.169/api/v1/updatelocation";
        String tag_json_obj = "updt_usr_loc";
        JSONObject params = new JSONObject();
        try {
            params.put("appVersion", "1.0");
            params.put("apiVersion", "1.0");
            params.put("longitude", longitude);
            params.put("latitude", latitude);
            params.put("place", place);
            params.put("location", location);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, URL, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                pDialog.hide();
                pDialog.setMessage("Signing In");
                try {
                    int resultcode = Integer.parseInt(response.getString("resultCode").toString());
                    if (resultcode == 1) {
                        UpdateLocationcode=1;
                    } else {
                        UpdateLocationcode=0;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.hide();

            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        return UpdateLocationcode;
    }
}
