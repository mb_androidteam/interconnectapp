package com.app.interconnect.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.interconnect.R;

/**
 * Created by hp on 20-02-2017.
 */

public class ViewDialog {

    public void showDialog(final Activity activity, String msg) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_SWIPE_TO_DISMISS);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialogue_listview);

        ListView messageList = (ListView)dialog.findViewById(R.id.message_list);
        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);

        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button btn_send = (Button) dialog.findViewById(R.id.btn_send);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(activity, "Message send", Toast.LENGTH_SHORT).show();
            }
        });

        dialog.show();

    }

    public void listConfig()
    {


    }
}
