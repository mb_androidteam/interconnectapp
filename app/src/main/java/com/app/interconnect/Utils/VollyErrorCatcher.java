package com.app.interconnect.Utils;

import android.app.Activity;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.interconnect.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hp on 03-03-2017.
 */

public class VollyErrorCatcher {
    UserPref up;

    public void StatusCodeChecker(int res, final Activity activity) {

        up = new UserPref(activity);
        JSONObject aa = null;
//        try {
//            aa = new JSONObject(new String(response.data));
//            String error = aa.getString("error");
//            Toast.makeText(activity, "" + error, Toast.LENGTH_SHORT).show();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
        switch (res) {
            case 401:

                String URL_refrshToken = "http://54.202.4.169/api/v1/refreshtoken";
                JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL_refrshToken, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response" + response);
                            int resultcode = Integer.parseInt(response.getString("resultCode").toString());

                            if (resultcode == 1) {

                                String token = response.getString("authToken");
                                up.SaveICtoken(token);
                                Toast.makeText(activity, "Account Refreshed", Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(activity, "Invalid User", Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }

                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        String token = up.GetICtoken().toString();
                        headers.put("Authorization", "Bearer " + token);
                        return headers;
                    }
                };
                AppController.getInstance().addToRequestQueue(jsonObjReq, "sendMessage");

                break;
            default:
                break;

        }
    }
}
