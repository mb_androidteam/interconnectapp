package com.app.interconnect.Utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import com.app.interconnect.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by hp on 02-03-2017.
 */

public class GotoLocationSettingsDialog {
    public void showDialog(Activity activity) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(activity);
        builderSingle.setIcon(R.drawable.splash_logo);
        builderSingle.setTitle("Interconnect");
        builderSingle.setMessage("Enable Location");

        builderSingle.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final Activity finalActivity = activity;
        builderSingle.setPositiveButton("Go To Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent onGPS = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                finalActivity.startActivity(onGPS);
                dialog.dismiss();
            }
        });
        builderSingle.show();

    }

}
