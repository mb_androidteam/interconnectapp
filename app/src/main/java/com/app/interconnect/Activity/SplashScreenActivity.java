package com.app.interconnect.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.interconnect.R;
import com.app.interconnect.Utils.UserPref;
import com.app.interconnect.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hp on 15-02-2017.
 */

public class SplashScreenActivity extends Activity {
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    Intent toContentView;
    String messageid = "";
    String notiType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        toContentView = new Intent(getApplicationContext(), MainActivity.class);//Navigation to main activity
        Intent in = getIntent();
        Bundle bdextra = in.getExtras();
        try {
            if (bdextra != null) {
                messageid = getIntent().getExtras().get("messageId").toString();
                notiType = getIntent().getExtras().get("notificationType").toString();
                String URL = "http://54.202.4.169/api/v1/getmessage?appVersion=1.0&apiVersion=1.0&messageid=" + messageid;
                JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("response" + response);
                            int resultcode = Integer.parseInt(response.getString("resultCode").toString());
                            if (resultcode == 1) {
                                JSONObject msgDetails = response.getJSONObject("messageType");//3types of msg
                                String msgTypeId = msgDetails.get("MessageTypeId").toString();
                                JSONObject msg = response.getJSONObject("messageDetails");
                                String description = msg.get("Description").toString();
                                toContentView.putExtra("MsgTypeId", msgTypeId);
                                toContentView.putExtra("MsgId", messageid);
                                toContentView.putExtra("notitype", notiType);
                                toContentView.putExtra("desc",description);

                            } else {
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }

                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        UserPref up = new UserPref(getApplicationContext());
                        String token = up.GetICtoken().toString();
                        headers.put("Authorization", "Bearer " + token);
                        return headers;
                    }
                };

                AppController.getInstance().addToRequestQueue(jsonObjReq, "sendMessage");

            } else {
                toContentView.putExtra("MsgTypeId", "0");
                toContentView.putExtra("MsgId", "0");
                toContentView.putExtra("notitype", "0");
                toContentView.putExtra("desc","0");
            }
        } catch (Exception e) {
            toContentView.putExtra("MsgTypeId", "0");
            toContentView.putExtra("MsgId", "0");
            toContentView.putExtra("notitype", "0");
            toContentView.putExtra("desc","0");
        }

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity


                startActivity(toContentView);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

}
