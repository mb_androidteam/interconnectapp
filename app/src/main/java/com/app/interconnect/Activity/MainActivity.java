package com.app.interconnect.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.interconnect.Adapters.RecyclerViewAdapter;
import com.app.interconnect.R;
import com.app.interconnect.Service.LocationUpdateService;
import com.app.interconnect.Utils.ApiCommonClass;
import com.app.interconnect.Utils.GotoLocationSettingsDialog;
import com.app.interconnect.Utils.ResponseDialog;
import com.app.interconnect.Utils.SendConnectDialog;
import com.app.interconnect.Utils.UserPref;
import com.app.interconnect.Utils.VollyErrorCatcher;
import com.app.interconnect.app.AppController;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationSource.OnLocationChangedListener, GoogleMap.OnMarkerClickListener {
    GoogleMap mMap;
    GoogleApiClient GoogleApiClient;
    //    LocationRequest mLoactionRequest;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    UserPref up;
    private double longitude;
    private double latitude;
    FloatingActionButton fab_connect, fab_reply;
    String isMarkerSelected = null;
    BottomSheetBehavior bottomSheetBehavior;
    ApiCommonClass apiCmnClass;
    Activity forConnect;
    String markerPlace;
    String markerLocalLocation;
    double markerLatitude;
    double markerLongitude;
    TextView tvMrkdSafe, tvPplsFrmhere;
    List<HashMap<String, String>> addressList = null;
    HashMap<String, String> adress;
    RecyclerView rv_list;
    private LinearLayoutManager layoutManager;
    RecyclerViewAdapter adapt;
    ProgressDialog pdDialog;
    TextView numTxt;
    LatLng FirstLatLng;
    View marker;
    VollyErrorCatcher vollyErrorCatcher;
    String loclLocation = null;
    String loclPlace = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        // Showing status
        if (status == ConnectionResult.SUCCESS) {
        }
//            tvStatus.setText(&quot;Google Play Services are available&quot;);
        else {

            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        }
        forConnect = this;


        apiCmnClass = new ApiCommonClass();
        //bottom sheet
        vollyErrorCatcher = new VollyErrorCatcher();
        View bottomSheet = findViewById(R.id.bottom_sheet1);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        bottomSheetBehavior.setPeekHeight(0);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        tvMrkdSafe = (TextView) findViewById(R.id.tv_pplSafe);
        tvPplsFrmhere = (TextView) findViewById(R.id.tv_pplHere);
        rv_list = (RecyclerView) findViewById(R.id.rv_Uname);
        pdDialog = new ProgressDialog(this);
        pdDialog.setMessage("loading");
        fab_connect = (FloatingActionButton) findViewById(R.id.fab_connect);
        fab_reply = (FloatingActionButton) findViewById(R.id.fab_reply);

        fab_connect.hide();
        fab_reply.hide();
//===========For custom marker........//
        marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_map_marker, null);
        numTxt = (TextView) marker.findViewById(R.id.txt_markerCount);
        //checkLocationPermission();
        int off = 0;
        //=====Checking if gps On or not
        try {
            off = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        if (off == 0) {
            GotoLocationSettingsDialog gls = new GotoLocationSettingsDialog();

            gls.showDialog(this);

//                Intent onGPS = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                startActivity(onGPS);
        }
        //=========\\//============//
        //shared preference
        up = new UserPref(getApplicationContext());
        String UserName = up.GetUserName();
        if (UserName.equals("0")) {
            Intent i = new Intent(this, GoogleSignIn.class);
            finish();//to kill main activity else bakpress will show if not sign in
            startActivity(i);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Getting data from intentMessagr service and building dialogue
        String TypeId = getIntent().getExtras().getString("MsgTypeId");
        String Id = getIntent().getExtras().getString("MsgId");
        String notificationType = getIntent().getExtras().getString("notitype");
        String descrip = getIntent().getExtras().getString("desc");
        int MsgTypeId = Integer.parseInt(TypeId);
        int msgid = Integer.parseInt(Id);
        int notiType = Integer.parseInt(notificationType);
        if (MsgTypeId == 0) {

        } else if (notiType == 1) {
            ResponseDialog rd = new ResponseDialog();
            rd.showDialog(this, MsgTypeId, msgid, descrip);
        }
        //this is for popping up bottom shet iff noti type is 2(reply recieved)

        if (notiType == 1 || notiType == 0) {
        } else {
            //Checking how many people marked safe

            String URL_msgDetail = "http://54.202.4.169/api/v1/getmessage?appVersion=1.0&apiVersion=1.0&messageid=" + msgid;
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL_msgDetail, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        System.out.println("response" + response);
                        int resultcode = Integer.parseInt(response.getString("resultCode").toString());
                        addressList = new ArrayList<HashMap<String, String>>();
                        JSONArray searchArray = response.getJSONArray("messageResponse");
                        if (resultcode == 1) {
                            JSONObject msgDetails = response.getJSONObject("messageDetails");//3types of msg
                            Double lati = (Double) msgDetails.get("Latitude");
                            Double longi = (Double) msgDetails.get("Longitude");
                            String incomingPlace = (String) msgDetails.get("Place");
                            LatLng makLatLng = new LatLng(lati, longi);

                            int posResponse = Integer.parseInt(response.getString("postiveResponse").toString());
                            tvMrkdSafe.setText(String.valueOf(posResponse));
                            int pplsHerecount = Integer.parseInt(response.getString("userCount").toString());
                            tvPplsFrmhere.setText(String.valueOf(pplsHerecount));
                            //-----Adding marker to current location
                            MarkerOptions markerOptions = new MarkerOptions();
                            markerOptions.position(makLatLng);
                            markerOptions.title(incomingPlace);
                            markerOptions.snippet(pplsHerecount + " people");
                            numTxt.setText("" + pplsHerecount);
                            markerOptions.position(makLatLng);
//                            markerOptions.title(place);
//                                    markerOptions.snippet(noOfpeople + " people");
                            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(forConnect, marker)));
                            moveMap(lati, longi);
                            for (int i = 0; i < searchArray.length(); i++) {
                                JSONObject test = searchArray.getJSONObject(i);
                                adress = new HashMap<String, String>();
                                String name = test.getString("FirstName");
                                String date = test.getString("UpdatedOn");
                                String msg = test.getString("Response");
                                String reply = test.getString("ReplyMessage");
                                adress.put("name", name);
                                adress.put("date", date);
                                adress.put("msg", msg);
                                adress.put("reply", reply);
                                addressList.add(adress);
                            }
                            rv_list.setHasFixedSize(true);
                            layoutManager = new LinearLayoutManager(getApplicationContext());
                            rv_list.setLayoutManager(layoutManager);
                            adapt = new RecyclerViewAdapter(getApplicationContext(), addressList);
                            rv_list.setAdapter(adapt);
                            adapt.notifyDataSetChanged();
                            bottomSheetBehavior.setPeekHeight(200);

                        } else {
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        Toast.makeText(MainActivity.this, "No internet Connection", Toast.LENGTH_SHORT).show();
                    } else {
                        int res = error.networkResponse.statusCode;
                        if (res == 401) {
                            vollyErrorCatcher.StatusCodeChecker(res, forConnect);
                        }
                    }
                }


            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    String token = up.GetICtoken().toString();
                    headers.put("Authorization", "Bearer " + token);
                    return headers;
                }
            };

            AppController.getInstance().addToRequestQueue(jsonObjReq, "sendMessage");

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        String fcmToken = up.GetFCMtoken();
        System.out.println("tokenis: " + fcmToken);

        //==========Bottom sheet close control listner
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    bottomSheetBehavior.setPeekHeight(0);
                }
            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {
            }
        });
        //=======Floating action button code========


        fab_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isMarkerSelected != null) {
                    SendConnectDialog ad = new SendConnectDialog();
                    ad.showDialog(forConnect, isMarkerSelected, markerLatitude, markerLongitude, markerLocalLocation, markerPlace);
//                    ad.CustomShow(forConnect, isMarkerSelected, markerLatitude, markerLongitude, markerLocalLocation, markerPlace);
                } else {
                    Toast.makeText(getApplicationContext(), "Please select a location marker", Toast.LENGTH_SHORT).show();
                }

            }
        });


        fab_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isMarkerSelected != null) {
                    loclLocation = markerLocalLocation.replace(" ", "%20");
                    loclPlace = markerPlace.replace(" ", "%20");
                    String URL_replyByPlace = "http://54.202.4.169/api/v1/getmessagebyplace?appVersion=1.0&apiVersion=1.0&place=" + loclPlace + "&longitude=" + markerLongitude + "&latitude=" + markerLatitude + "&location=" + loclLocation;
                    JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL_replyByPlace, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                System.out.println("response" + response);
                                int resultcode = Integer.parseInt(response.getString("resultCode").toString());
                                addressList = new ArrayList<HashMap<String, String>>();
                                JSONArray searchArray = response.getJSONArray("messageResponse");

                                if (resultcode == 1) {
                                    int posResponse = Integer.parseInt(response.getString("postiveResponse").toString());
                                    tvMrkdSafe.setText(String.valueOf(posResponse));
                                    int pplsHerecount = Integer.parseInt(response.getString("userCount").toString());
                                    tvPplsFrmhere.setText(String.valueOf(pplsHerecount));
                                    bottomSheetBehavior.setPeekHeight(200);
                                    for (int i = 0; i < searchArray.length(); i++) {
                                        JSONObject test = searchArray.getJSONObject(i);
                                        adress = new HashMap<String, String>();
                                        String name = test.getString("FirstName");
                                        String date = test.getString("UpdatedOn").toString();
                                        String msg = test.getString("Response");
                                        String replay = test.get("ReplyMessage").toString();
                                        adress.put("name", name);
                                        adress.put("date", date);
                                        adress.put("msg", msg);
                                        adress.put("replay", replay);
                                        addressList.add(adress);
                                    }
                                    rv_list.setHasFixedSize(true);
                                    layoutManager = new LinearLayoutManager(getApplicationContext());
                                    rv_list.setLayoutManager(layoutManager);
                                    adapt = new RecyclerViewAdapter(getApplicationContext(), addressList);
                                    rv_list.setAdapter(adapt);
                                    adapt.notifyDataSetChanged();


                                } else {

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error.networkResponse == null) {
                                Toast.makeText(MainActivity.this, "No internet Connection", Toast.LENGTH_SHORT).show();
                            } else {
                                int res = error.networkResponse.statusCode;
                                if (res == 401) {
                                    vollyErrorCatcher.StatusCodeChecker(res, forConnect);
                                }
                            }
                            Toast.makeText(forConnect, "You have not connected to this place yet", Toast.LENGTH_SHORT).show();

                        }

                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<String, String>();
                            String token = up.GetICtoken().toString();
                            headers.put("Authorization", "Bearer " + token);
                            return headers;
                        }
                    };

                    AppController.getInstance().addToRequestQueue(jsonObjReq, "getMessageby_place");

                } else {
                    Toast.makeText(forConnect, "Please select a marker", Toast.LENGTH_SHORT).show();
                }


            }
        });

        ///========///=================///===============///

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //=====FOR MAPS
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
//        checkLocationPermission();

        //==========

        //=======Auto Complete text


        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                //=====CODE FOR GETTING NAME AND SETTING LOCATION MARKER=======//
                LatLng latLng1 = place.getLatLng();
                FindNearByPlaces(latLng1.latitude, latLng1.longitude, latLng1);
            }


            //
            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Toast.makeText(MainActivity.this, "Error loading place", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void getLocationInfo(Double lat, Double lon) {
        Geocoder gcd = new Geocoder(this);
        List<Address> addresses = null;
        String name;
        try {
            addresses = gcd.getFromLocation(lat, lon, 1);
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
            Toast.makeText(this, "" + knownName, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_ntification) {
            Intent toNotification = new Intent(this, Notification.class);
            startActivity(toNotification);

        } else if (id == R.id.nav_share) {

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Interconnect URL");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } else if (id == R.id.nav_logout) {
            Toast.makeText(this, "Logged out", Toast.LENGTH_SHORT).show();
            String URL_logout = "http://54.202.4.169/api/v1/logout";
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL_logout, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        int resultcode = Integer.parseInt(response.getString("resultCode").toString());

                        if (resultcode == 1) {
                            up.Clear();
                        } else {

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse == null) {
                        Toast.makeText(MainActivity.this, "No internet Connection", Toast.LENGTH_SHORT).show();
                    } else {
                        int res = error.networkResponse.statusCode;
                        if (res == 401) {
                            vollyErrorCatcher.StatusCodeChecker(res, forConnect);
                        }
                    }
                }

            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    String token = up.GetICtoken().toString();
                    headers.put("Authorization", "Bearer " + token);
                    return headers;
                }
            };

            AppController.getInstance().addToRequestQueue(jsonObjReq, "getMessageby_place");

            Intent in = new Intent(this, GoogleSignIn.class);
            finish();
            startActivity(in);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(googleMap.MAP_TYPE_NORMAL);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                fab_reply.hide();
                fab_connect.hide();
            }
        });
        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.

                }
                Location location = LocationServices.FusedLocationApi.getLastLocation(GoogleApiClient);
                if (location != null) {
                    //Getting longitude and latitude
                    double longi = location.getLongitude();
                    double lati = location.getLatitude();
                    LatLng latlng = new LatLng(lati, longi);

                    FindNearByPlaces(lati, longi, latlng);
                    //moving the map to location

                } else {

                }
                return false;

            }
        });
        mMap.setOnMarkerClickListener(this);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                GoogleApiClient.connect();
                mMap.setMyLocationEnabled(true);
//                mMap.animateCamera( CameraUpdateFactory.zoomTo(15) );
            }
        } else {
            buildGoogleApiClient();
            GoogleApiClient.connect();
            mMap.setMyLocationEnabled(true);

        }
    }

    private void buildGoogleApiClient() {
        GoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // Permission was granted.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (GoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }

            }

            // other 'case' lines to check for other permissions this app might request.
            //You can add here other case statements according to your requirement.
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getCurrentLocation();
        moveMap(latitude, longitude);
        FirstLatLng = new LatLng(latitude, longitude);
        FindNearByPlaces(latitude, longitude, FirstLatLng);
        startService(new Intent(this, LocationUpdateService.class));
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
    }

    private void getCurrentLocation() {
        mMap.clear();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(GoogleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            //moving the map to location
        } else {

        }
    }


    private void moveMap(double latitude, double longitude) {
        /**
         * Creating the latlng object to store lat, long coordinates
         * move the camera with animation
         */
        LatLng latLng = new LatLng(latitude, longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(21));
//        mMap.getUiSettings().setZoomControlsEnabled(false);


    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        fab_reply.show();
        fab_connect.show();

        isMarkerSelected = marker.getTitle().toString();
        LatLng markerLatLng = marker.getPosition();
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.ENGLISH);
        List<Address> addresses = null;
        try {
            markerLatitude = markerLatLng.latitude;
            markerLongitude = markerLatLng.longitude;
            addresses = geocoder.getFromLocation(markerLatitude, markerLongitude, 1);
            markerPlace = addresses.get(0).getLocality();
            markerLocalLocation = addresses.get(0).getFeatureName().toString();
        } catch (Exception ex) {

        }

//        marker.getPosition();
        return false;
    }

    public void FindNearByPlaces(Double latitude, Double longitude, LatLng latlng) {
        pdDialog.show();
        String URL_getNrloctn = "http://54.202.4.169/api/v1/getnearlocation?appVersion=1.0&apiVersion=1.0&longitude=" + longitude + "&latitude=" + latitude + "&distance=10";
//                String URL_getNrloctn = "http://54.202.4.169/api/v1/getnearlocation?appVersion=1.0&apiVersion=1.0&longitude=76.3193939&latitude=9.96951&distance=10";
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(20));
        mMap.clear();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL_getNrloctn, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                        System.out.println("Report" + response);
//                        Log.e("Mytag",":"+response);
                try {
                    int resultcode = Integer.parseInt(response.getString("resultCode").toString());
                    if (resultcode == 1) {
                        JSONArray locationArray = response.getJSONArray("locations");
                        JSONObject location;
                        if (locationArray != null) {
                            for (int i = 0; i < locationArray.length(); i++) {
                                location = locationArray.getJSONObject(i);
                                int noOfpeople = Integer.parseInt(location.getString("NoOfPeoples"));
                                if (noOfpeople > 0) {
                                    double inner_latitude = Double.parseDouble(location.getString("Latitude"));
                                    double inner_logitude = Double.parseDouble(location.getString("Longitude"));
                                    String place = location.getString("Location");
                                    LatLng markme = new LatLng(inner_latitude, inner_logitude);
                                    MarkerOptions markerOptions = new MarkerOptions();


                                    numTxt.setText("" + noOfpeople);
                                    markerOptions.position(markme);
                                    markerOptions.title(place);
//                                    markerOptions.snippet(noOfpeople + " people");
                                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(forConnect, marker)));
                                    mCurrLocationMarker = mMap.addMarker(markerOptions);
                                    mCurrLocationMarker.showInfoWindow();


                                }
                            }
                        }
                        pdDialog.dismiss();

                    } else {
                        pdDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "No users found in this location", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    pdDialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                pdDialog.dismiss();
                if (error.networkResponse == null) {
                    Toast.makeText(MainActivity.this, "No internet Connection", Toast.LENGTH_SHORT).show();
                } else {
                    int res = error.networkResponse.statusCode;
                    if (res == 401) {
                        vollyErrorCatcher.StatusCodeChecker(res, forConnect);
                    }
                }
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> head = new HashMap<String, String>();
                String getToken = up.GetICtoken();
                String aa = "Bearer ".concat(getToken);
                head.put("Authorization", aa);
                return head;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjReq, "Get_near_location");
    }

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new DrawerLayout.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }


}




