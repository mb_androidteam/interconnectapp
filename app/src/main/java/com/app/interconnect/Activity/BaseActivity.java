package com.app.interconnect.Activity;


import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.app.interconnect.R;
import com.app.interconnect.Utils.BusFactory;

/**
 * Created by Sanif on 03-01-2016.
 */
public class BaseActivity extends AppCompatActivity {


    private OnBackPressedListener backPressedListener;
    private Toolbar mToolBar;
    private ProgressDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusFactory.getBus().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BusFactory.getBus().unregister(this);
    }

    public Toolbar getToolBar() {
        return mToolBar;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolBar != null)
            setSupportActionBar(mToolBar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("");
    }

    public void setActionBarTransparent() {
        if (mToolBar != null) {
            mToolBar.setBackgroundColor(Color.argb(255, 0, 0, 0));
        }
    }

    public void setOnBackPressedListener(OnBackPressedListener listener) {
        backPressedListener = listener;
    }

    /**
     * Override this method to handle the back stack callbacks.
     */
    public void onBackStackChanged() {

    }

    /**
     * Method to add fragment to the container.
     *
     * @param containerId:    The id of the container.
     * @param fragment:       The fragment to add.
     * @param addToBackStack: Whether to add to backstack or not.
     */
    public void addFragment(int containerId, Fragment fragment, boolean addToBackStack) {

        if (isCurrentFragment(fragment)) {//no need to add a fragment twice
            return;
        }

        FragmentTransaction fTransaction = getSupportFragmentManager().beginTransaction();
        fTransaction.add(containerId, fragment, fragment.getClass().getSimpleName());//Tag : used for findFragmentByTag
        if (addToBackStack) {
            fTransaction.addToBackStack(null);
        }
        fTransaction.commit();
    }

    /**
     * Method to add fragment to the container with id R.id.container.
     *
     * @param fragment:       The fragment to add.
     * @param addToBackStack: Whether to add to backstack or not.
     */
    public void addFragment(Fragment fragment, boolean addToBackStack) {
        addFragment(R.id.container, fragment, addToBackStack);
    }

    /**
     * Method to replace fragment in a container.
     *
     * @param containerId:    The id of the container.
     * @param fragment:       The fragment to replace.
     * @param addToBackStack: Whether to add to backstack or not.
     */
    public void replaceFragment(int containerId, Fragment fragment, boolean addToBackStack, boolean animate) {

        if (isCurrentFragment(fragment)) {//no need to add a fragment twice
            return;
        }

        FragmentTransaction fTransaction = getSupportFragmentManager().beginTransaction();
        if (animate) {
//            fTransaction.setCustomAnimations(R.anim.move_right_in_activity, R.anim.nothing_anim);
        }
        fTransaction.replace(containerId, fragment, fragment.getClass().getSimpleName());//Tag : used for findFragmentByTag
        if (addToBackStack) {
            fTransaction.addToBackStack(null);
        }
        fTransaction.commit();

    }

    public void replaceFragment(Fragment fragment, boolean addToBackStack) {
        replaceFragment(R.id.container, fragment, addToBackStack, true);
    }

    public Fragment getFragment(Class clazz) {
        return getSupportFragmentManager().findFragmentByTag(clazz.getSimpleName());
    }

    public void popBackStack() {
        getSupportFragmentManager().popBackStack();
    }

    /**
     * To check whether the given fragment is the current fragment.
     *
     * @param fragment
     * @return
     */
    public boolean isCurrentFragment(Fragment fragment) {
        Fragment tFragment = getFragment(fragment.getClass());
        if (tFragment != null && tFragment.isVisible()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //to handel the actionbar back button
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showLoading(String message) {
        if (pDialog == null) {
            pDialog = new ProgressDialog(this);
            pDialog.setCancelable(false);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.setMessage(message);
        }
        pDialog.show();
    }

    public void showLoading() {
        showLoading("Loading..");
    }

    public void hideLoading() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    //Nested fragments backstack workaround
    private boolean onBackPressed(FragmentManager fm) {

        return false;
    }

    @Override
    public void onBackPressed() {
        if (backPressedListener == null || !backPressedListener.onBackPressed()) {

            FragmentManager fm = getSupportFragmentManager();
            if (onBackPressed(fm)) {
                return;
            }
            super.onBackPressed();
        }
    }


    public interface OnBackPressedListener {
        boolean onBackPressed();
    }

}
