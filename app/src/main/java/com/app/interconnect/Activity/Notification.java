package com.app.interconnect.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.interconnect.Adapters.NotificationAdapter;
import com.app.interconnect.Adapters.RecyclerViewAdapter;
import com.app.interconnect.R;
import com.app.interconnect.Utils.UserPref;
import com.app.interconnect.Utils.VollyErrorCatcher;
import com.app.interconnect.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hp on 15-03-2017.
 */

public class Notification extends Activity{
    List<HashMap<String, String>> addressList;
    HashMap<String, String> adress;
    RecyclerView rv_list;
    LinearLayoutManager layoutManager;
    NotificationAdapter adapt;
    VollyErrorCatcher vollyErrorCatcher;
    Activity a;
    UserPref up;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification);
        a=this;
        rv_list = (RecyclerView)findViewById(R.id.rv_notifications);
        up = new UserPref(getApplicationContext());
        pd= new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.show();
        String URL_replyByPlace = "http://54.202.4.169/api/v1/getmessages?appVersion=1.0&apiVersion=1.0&page=1";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL_replyByPlace, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    int resultcode = Integer.parseInt(response.getString("resultCode").toString());
                    addressList = new ArrayList<HashMap<String, String>>();

                    if (resultcode == 1) {
                        JSONObject messageDetailsObject=response.getJSONObject("messageDetails");
                        JSONArray searchArray = messageDetailsObject.getJSONArray("data");

                        for (int i = 0; i < searchArray.length(); i++) {
                            JSONObject test = searchArray.getJSONObject(i);
                            adress = new HashMap<String, String>();
                            String place = test.getString("Place");
                            String date = test.getString("CreatedOn").toString();
                            String description = test.getString("Description");
                            String msgId = test.getString("MessageId");
                            adress.put("place", place);
                            adress.put("date", date);
                            adress.put("description", description);
                            adress.put("msgid",msgId);
                            addressList.add(adress);
                        }
                        rv_list.setHasFixedSize(true);
                        layoutManager = new LinearLayoutManager(getApplicationContext());
                        rv_list.setLayoutManager(layoutManager);
                        adapt = new NotificationAdapter(Notification.this, addressList);
                        rv_list.setAdapter(adapt);
                        adapt.notifyDataSetChanged();
                        pd.dismiss();


                    } else {
                            pd.dismiss();
                        Toast.makeText(a, "You haven't Send any connect requests", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    pd.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                if(error.networkResponse==null)
                {
                    Toast.makeText(a, "No internet Connection", Toast.LENGTH_SHORT).show();
                }
                else {
                    int res = error.networkResponse.statusCode;
                    if (res == 401) {
                        vollyErrorCatcher.StatusCodeChecker(res, a);
                    }
                }
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                String token = up.GetICtoken().toString();
                System.out.println("hed "+token);
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjReq, "notification");
    }
}
