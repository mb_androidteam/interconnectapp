package com.app.interconnect.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.app.interconnect.R;
import com.app.interconnect.Utils.BusFactory;

/**
 * Created by Sanif on 04-01-2016.
 */
public class BaseFragment extends Fragment {


    private boolean enableBackButton = true;
    private TextView toolBarTitle, toolBarAction;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusFactory.getBus().register(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolbar(view);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusFactory.getBus().unregister(this);

    }

    /**
     * This method is called to set the Toolbar with id 'toolbar' as ActionBar
     * Override this method to give custom implementation
     *
     * @param v: The container view
     */
    public void setupToolbar(View v) {
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolBarTitle = (TextView) v.findViewById(R.id.tvTitle);

            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
            if (getTitle() != 0 && toolBarTitle != null) {
                toolBarTitle.setText(getTitle());
            }

        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(null);//for disabling default title shown in action bar
            if (enableBackButton) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            }
        }
    }

    /**
     * override this method to give title to toolBar
     *
     * @return
     */
    public
    @StringRes
    int getTitle() {
        return 0;
    }

    public
    @StringRes
    int getAction() {
        return 0;
    }

    public ActionBar getSupportActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {

            getActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setEnableBackButton(boolean enableBackButton) {
        this.enableBackButton = enableBackButton;
    }


}
