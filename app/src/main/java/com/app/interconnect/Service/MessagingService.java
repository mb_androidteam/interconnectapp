package com.app.interconnect.Service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.interconnect.Activity.MainActivity;
import com.app.interconnect.R;
import com.app.interconnect.Utils.UserPref;
import com.app.interconnect.app.AppController;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hp on 19-02-2017.
 */

public class MessagingService extends FirebaseMessagingService {
    String msgTypeId;
    String advTitle;
    String MsgText;
    String messageid;
    String notificationtype;
    String descrip;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        advTitle = remoteMessage.getNotification().getTitle();

        MsgText = remoteMessage.getNotification().getBody();
        Map<String, String> on = remoteMessage.getData();
        messageid = on.get("messageId");
        notificationtype = on.get("notificationType");
        System.out.println("noti" + on);
        String URL = "http://54.202.4.169/api/v1/getmessage?appVersion=1.0&apiVersion=1.0&messageid=" + messageid;
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    System.out.println("response" + response);
                    int resultcode = Integer.parseInt(response.getString("resultCode").toString());
                    if (resultcode == 1) {
                        JSONObject msgDetails = response.getJSONObject("messageType");
                        JSONObject msg = response.getJSONObject("messageDetails");
                        descrip = msg.get("Description").toString();
                        msgTypeId = msgDetails.get("MessageTypeId").toString();
                        sendNotification(MsgText, advTitle, msgTypeId);
                    } else {
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                UserPref up = new UserPref(getApplicationContext());
                String token = up.GetICtoken().toString();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjReq, "sendMessage");

    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    private void sendNotification(String msgText, String title, String messageTypeId) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        System.out.println(messageTypeId + "messageTypeId");
        intent.putExtra("MsgTypeId", messageTypeId);
        intent.putExtra("MsgId", messageid);
        intent.putExtra("notitype", notificationtype);
        intent.putExtra("desc",descrip);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.main_logo)
                .setContentTitle(title)
                .setContentText(msgText)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
