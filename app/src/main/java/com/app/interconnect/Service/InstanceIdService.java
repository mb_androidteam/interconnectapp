package com.app.interconnect.Service;

import com.app.interconnect.Utils.UserPref;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by hp on 19-02-2017.
 */

public class InstanceIdService extends FirebaseInstanceIdService {
UserPref up;
    @Override
    public void onTokenRefresh() {
        up = new UserPref(getApplicationContext());
        super.onTokenRefresh();
        String refreshToken = FirebaseInstanceId.getInstance().getToken();
        up.SaveFCMtoken(refreshToken);
    }

}